package edu.upec.m2.client;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

class StringUtilsTest {
	@Test
	public void whenCalledisBlank__thenCorrect() {
	    assertTrue(StringUtils.isBlank(" "));
	}

	@Test
	public void whenCalledisEmpty__thenCorrect() {
		assertTrue(StringUtils.isEmpty(""));
	}

	@Test
	public void whenCalledisAllLowerCase__thenCorrect() {
		assertTrue(StringUtils.isAllLowerCase("abd"));
	}

	@Test
	public void whenCalledisAllUpperCase__thenCorrect() {
		assertTrue(StringUtils.isAllUpperCase("ABC"));
	}

	@Test
	public void whenCalledisMixedCase__thenCorrect() {
		assertTrue(StringUtils.isMixedCase("abC"));
	}

	@Test
	public void whenCalledisAlpha__thenCorrect() {
		assertTrue(StringUtils.isAlpha("abc"));
	}

	@Test
	public void whenCalledisAlphanumeric__thenCorrect() {
		assertTrue(StringUtils.isAlphanumeric("abc123"));
	}

}
