package edu.upec.m2.model;

public class PartTimeEmployee extends CompanyEmployee{
	private float hourlyRate;

	public float getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(float hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	
	
}
