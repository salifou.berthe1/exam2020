package edu.upec.m2.client;

import java.sql.SQLException;

public class H2Viewer {
  public static void main(String[] args) throws SQLException {
	  org.h2.tools.Console.main("-url","jdbc:h2:file:~/exam2020", "-user", "upec", "-password", "upec");
  }
}
